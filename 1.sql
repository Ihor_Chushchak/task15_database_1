USE Labor_SQL;
/*1*/
SELECT maker,type FROM Product
ORDER BY maker;
/*2*/
SELECT ram,model,screen,price FROM Laptop
WHERE price >1000
ORDER BY ram ,price DESC;
/*3*/
SELECT * FROM Printer
WHERE color = 'y'
ORDER BY price DESC;
/*4*/
SELECT model,speed,hd,cd,price FROM PC
WHERE (cd='12x' OR cd='24x') AND price<600
ORDER BY speed;
/*5*/
SELECT name,class FROM Ships
ORDER BY name;
/*6*/
SELECT speed,price FROM PC
WHERE speed>=500 AND price<800
ORDER BY price DESC;
/*7*/
SELECT * FROM Printer
WHERE type<>'Matrix'
ORDER BY type DESC;
/*8*/
SELECT model,speed FROM PC
WHERE price BETWEEN 400 AND 600
ORDER BY hd;
/*9*/
SELECT PC.model,speed,hd FROM PC
INNER JOIN Product ON PC.model=Product.model
WHERE Product.maker='A' AND (PC.hd=10 OR PC.hd=20)
ORDER BY speed;
/*10*/
SELECT model,speed,hd,price FROM Laptop
WHERE screen>=12
ORDER BY price DESC;
/*11*/
SELECT model,type,price FROM Printer
WHERE price<300
ORDER BY type DESC;
/*12*/
SELECT model,ram,price FROM Laptop
WHERE ram=64
ORDER BY screen;
/*13*/
SELECT model FROM PC
WHERE ram>64
ORDER BY hd;
/*14*/
SELECT model,speed,price FROM PC
WHERE speed BETWEEN 500 AND 750
ORDER BY hd DESC;
/*15*/
SELECT O.point,O.date,O.out FROM Outcome_o O
WHERE O.out>2000
ORDER BY date DESC;
/*16*/
SELECT O.point,O.date,O.inc FROM Income_o O
WHERE O.inc BETWEEN 5000 AND 10000
ORDER BY inc;
/*17*/
SELECT O.point,O.date,O.inc FROM Income O
WHERE O.point=1
ORDER BY inc;
/*18*/
SELECT O.point,O.date,O.out FROM Outcome O
WHERE O.point=2
ORDER BY O.out;
/*19*/
SELECT * FROM Classes
WHERE country='Japan'
ORDER BY Classes.type DESC;
/*20*/
SELECT name,launched FROM Ships
WHERE launched BETWEEN 1920 AND 1942
ORDER BY launched DESC;
/*21*/
SELECT ship,battle,result FROM Outcomes
WHERE battle='Guadalcanal' AND result<>'sunk'
ORDER BY ship DESC;
/*22*/
SELECT ship,battle,result FROM Outcomes
WHERE result='sunk'
ORDER BY ship DESC;
/*23*/
SELECT class,displacement FROM Classes
WHERE displacement>=40000
ORDER BY type;
/*24*/
SELECT trip_no, town_from, town_to FROM Trip
WHERE town_to='London'
ORDER BY time_out;
/*25*/
SELECT trip_no,plane, town_from, town_to FROM Trip
WHERE plane='TU-134'
ORDER BY time_out DESC;
/*26*/
SELECT trip_no,plane, town_from, town_to FROM Trip
WHERE plane<>'IL-86'
ORDER BY plane;
/*27*/
SELECT trip_no,plane, town_from, town_to FROM Trip
WHERE town_to<>'Rostov'
ORDER BY plane;



 


