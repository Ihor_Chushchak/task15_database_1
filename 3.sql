/*1*/
SELECT DISTINCT P.maker,P.type,PC.speed,PC.hd FROM Product P
INNER JOIN PC ON P.model=PC.model
WHERE PC.hd<=8;
/*2*/
SELECT P.maker,PC.speed FROM Product P
INNER JOIN PC ON P.model=PC.model
WHERE PC.speed>=600;
/*3*/
SELECT P.maker,L.speed FROM Product P
INNER JOIN Laptop L ON P.model=L.model
WHERE L.speed<=500;
/*4*/
SELECT DISTINCT P.model,L.model,P.hd,P.ram FROM Laptop P,Laptop L
WHERE L.ram=P.ram AND L.code<>P.code AND L.hd=P.hd
ORDER BY P.model DESC,L.model;
/*5*/
SELECT country FROM Classes 
WHERE type='bb'
AND country 
IN 
(SELECT country FROM classes 
	WHERE type='bc'
	GROUP BY country) 
GROUP BY country;
/*6*/
SELECT P.model,maker FROM Product P
INNER JOIN PC ON P.model=PC.model
WHERE PC.price<600;
/*7*/
SELECT P.model,maker FROM Product P
INNER JOIN Printer Pr ON P.model=Pr.model
WHERE Pr.price>300;
/*8*/
SELECT P.model,maker,L.price FROM Product P
INNER JOIN Laptop L ON P.model=L.model;
/*9*/
SELECT P.model,maker,PC.price FROM Product P
INNER JOIN PC ON P.model=PC.model;
/*10*/
SELECT P.maker,P.type,P.model,speed FROM Product P
INNER JOIN Laptop L ON P.model=L.model
WHERE L.speed>600;
/*11*/
SELECT S.name,displacement FROM Ships S 
INNER JOIN Classes C ON S.class=C.class;
/*12*/
SELECT B.name,date,result FROM Battles B 
INNER JOIN Outcomes O ON B.name=O.battle
WHERE result='OK';
/*13*/
SELECT name,country FROM Ships S 
INNER JOIN Classes C ON S.class=C.class; 
/*14*/
SELECT DISTINCT C.ID_comp,name,plane FROM Company C 
INNER JOIN Trip T ON C.ID_comp=T.ID_comp
WHERE plane='Boeing';
/*15*/
SELECT name,date FROM Passenger P 
INNER JOIN Pass_in_trip Ps ON P.ID_psg=Ps.ID_psg;