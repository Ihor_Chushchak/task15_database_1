/*1*/
SELECT model FROM PC
WHERE model RLIKE '[1]+[0-9]([1]+)';
/*2*/
SELECT * FROM Outcome
WHERE Month(date)=3;
/*3*/
SELECT * FROM Outcome_o
WHERE Day(date)=14;
/*4*/
SELECT name FROM Ships
WHERE name RLIKE '^W.[a-z]+[n$]';
/*5*/
SELECT name FROM Ships
WHERE name RLIKE '^[^e]*e[^e]*e[^e]*$';
/*6*/
SELECT name,launched FROM Ships
WHERE name RLIKE '[^a]$';
/*7*/
SELECT name FROM Battles
WHERE name RLIKE'[^ ]* [^ ]*[^c]$';
/*8*/
SELECT * FROM Trip
WHERE HOUR(time_out) BETWEEN 12 AND 17;
/*9*/
SELECT * FROM Trip
WHERE HOUR(time_in) BETWEEN 17 AND 23;
/*10*/
SELECT date FROM Pass_in_trip
WHERE place RLIKE '^[1].*';
/*11*/
SELECT date,place FROM Pass_in_trip
WHERE place RLIKE '^[^c][c]$';
/*12*/
SELECT name FROM Passenger
WHERE name RLIKE '[^ ][ ][ ][C]{1}.*';
/*13*/
SELECT name FROM Passenger
WHERE name RLIKE '^[^ ]* [^J][^ ]*';


