-- MySQL Script generated by MySQL Workbench
-- Tue Oct 22 23:10:43 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Прометеус
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Прометеус
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Прометеус` DEFAULT CHARACTER SET utf8 ;
USE `Прометеус` ;

-- -----------------------------------------------------
-- Table `Прометеус`.`Group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Group` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Amount_of_students` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Прометеус`.`Student_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Student_status` (
  `Id` INT NOT NULL,
  `Description` VARCHAR(45) NULL,
  `Student_status` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Прометеус`.`Student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Student` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Surname` VARCHAR(45) NULL,
  `Phone_number` VARCHAR(45) NULL,
  `Email` VARCHAR(45) NULL,
  `Gender` VARCHAR(45) NULL,
  `Group_Id` INT NOT NULL,
  `Student_status_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Student_Group1_idx` (`Group_Id` ASC) VISIBLE,
  INDEX `fk_Student_Student_status1_idx` (`Student_status_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Student_Group1`
    FOREIGN KEY (`Group_Id`)
    REFERENCES `Прометеус`.`Group` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Student_Student_status1`
    FOREIGN KEY (`Student_status_Id`)
    REFERENCES `Прометеус`.`Student_status` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Прометеус`.`Course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Course` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Start` DATE NULL,
  `Finish` DATE NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Прометеус`.`Mentor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Mentor` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Surname` VARCHAR(45) NULL,
  `Email` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Прометеус`.`Module`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Module` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Mark` VARCHAR(45) NULL,
  `Course_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Module_Course1_idx` (`Course_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Module_Course1`
    FOREIGN KEY (`Course_Id`)
    REFERENCES `Прометеус`.`Course` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Прометеус`.`Test`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Test` (
  `Id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Date` DATE NULL,
  `Mark` INT NULL,
  `Course_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Test_Course1_idx` (`Course_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Test_Course1`
    FOREIGN KEY (`Course_Id`)
    REFERENCES `Прометеус`.`Course` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Прометеус`.`Warning`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Warning` (
  `Id` INT NOT NULL,
  `Type` VARCHAR(45) NULL,
  `To` VARCHAR(45) NULL,
  `Date` VARCHAR(45) NULL,
  `Student_Id` INT NOT NULL,
  `Student_Group_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Warning_Student1_idx` (`Student_Id` ASC, `Student_Group_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Warning_Student1`
    FOREIGN KEY (`Student_Id` , `Student_Group_Id`)
    REFERENCES `Прометеус`.`Student` (`Id` , `Group_Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Прометеус`.`Course_has_Student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Course_has_Student` (
  `Course_Id` INT NOT NULL,
  `Student_Id` INT NOT NULL,
  INDEX `fk_Course_has_Student_Student1_idx` (`Student_Id` ASC) VISIBLE,
  INDEX `fk_Course_has_Student_Course1_idx` (`Course_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Course_has_Student_Course1`
    FOREIGN KEY (`Course_Id`)
    REFERENCES `Прометеус`.`Course` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Course_has_Student_Student1`
    FOREIGN KEY (`Student_Id`)
    REFERENCES `Прометеус`.`Student` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Прометеус`.`Course_has_Mentor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Course_has_Mentor` (
  `Course_Id` INT NOT NULL,
  `Mentor_Id` INT NOT NULL,
  INDEX `fk_Course_has_Mentor_Mentor1_idx` (`Mentor_Id` ASC) VISIBLE,
  INDEX `fk_Course_has_Mentor_Course1_idx` (`Course_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Course_has_Mentor_Course1`
    FOREIGN KEY (`Course_Id`)
    REFERENCES `Прометеус`.`Course` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Course_has_Mentor_Mentor1`
    FOREIGN KEY (`Mentor_Id`)
    REFERENCES `Прометеус`.`Mentor` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Прометеус`.`Hometask`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Прометеус`.`Hometask` (
  `Id` INT NOT NULL,
  `Type` VARCHAR(45) NULL,
  `Description` VARCHAR(45) NULL,
  `Task` VARCHAR(45) NULL,
  `Course_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Hometask_Course1_idx` (`Course_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Hometask_Course1`
    FOREIGN KEY (`Course_Id`)
    REFERENCES `Прометеус`.`Course` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
